class Number {
  factory Number(String number) {
    if (number.length == 1) return Number._single(int.parse(number));
    final list = number.split('~');
    if (list.length != 2) throw StateError('Invalid Input');
    return Number._(int.parse(list.first), int.parse(list[1]));
  }

  const Number._(this.min, this.max);

  const Number._single(int number)
      : this.min = number,
        this.max = number;

  final int max;
  final int min;

  operator +(Number other) {
    return Number._(min + other.min, max + other.max);
  }

//  double avg() => (max + min) / 2;
}
