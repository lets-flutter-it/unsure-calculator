import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_bloc.dart';
import 'package:unsurecalculator/screens/home_screen/home_screen.dart';
import 'package:unsurecalculator/screens/splash_screen.dart';

import 'blocs/authentecation/auth_event.dart';
import 'blocs/authentecation/auth_bloc_state.dart';
import 'screens/home_screen/home_screen.dart';
import 'screens/login_screen.dart';

class AppBase extends StatelessWidget {
  const AppBase();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AuthBloc()..add(StartApp()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.orange,
          primaryColorLight: Colors.orange,
        ),
        home: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              Navigator.of(context).popUntil((route) => route.isFirst);
            });
            return state.when(
                loadingApp: () => SplashScreen(),
                authenticated: (user) => HomeScreen(user),
                unauthenticated: () => LoginScreen());
          },
        ),
      ),
    );
  }
}
