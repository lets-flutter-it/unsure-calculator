
import 'package:flutter/material.dart';

import '../screens/register_screen.dart';
import '../screens/register_screen.dart';
class Button extends StatelessWidget {
  const Button(this.text , this.color, this.txt_color,{ @required this.onPressed});
  final String text;
  final Color color;
  final Color txt_color;
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      child: RaisedButton(
        child: Text(
          text,
          style: TextStyle(fontSize: 20),
        ),
        color: color,
        textColor: txt_color,
        onPressed:()=> onPressed(),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30) ,
        ),
      ),
    );
    }
}
