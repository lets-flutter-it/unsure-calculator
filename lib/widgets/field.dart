import 'package:flutter/material.dart';
class Field extends StatelessWidget {
   const Field(this.hint ,this.icon ,this.controller,{this.obscure = false});
   final String hint;
   final IconData icon;
   final bool obscure;
   final TextEditingController controller;

  @override
  Widget build(BuildContext context) => TextField(
    controller: controller,
    obscureText: obscure,
    decoration: InputDecoration(
        hintText: hint,
        prefixIcon: Icon(icon)),
  );
}
