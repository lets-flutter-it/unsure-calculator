import 'package:flutter/material.dart';

class History extends StatelessWidget {
  const History(this.historyList);
  final List<String> historyList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('History'),
          backgroundColor: Colors.orange,
        ),
        body: Center(
          child: historyList?.isEmpty ?? true
              ? Text("no history")
              : ListView.separated(
                  itemCount: historyList.length,
                  padding: const EdgeInsets.all(16),
                  separatorBuilder: (_, __) => Divider(),
                  itemBuilder: (context, item) {
                    final index = item ~/ 2;
                    final equation = historyList[item].split('=');
                    return ListTile(
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            equation[0],
                            style: TextStyle(
                              color: Colors.black38,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            equation[1],
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.orange
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
        ));
  }
}
