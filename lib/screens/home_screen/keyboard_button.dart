import 'package:flutter/material.dart';

class KeyBoardButton extends StatelessWidget {
  const KeyBoardButton(this.character,this.textcolor , this.color , {@required this.onPressed});

  final String character;
  final Color textcolor;
  final Color color;
  final void Function(String number) onPressed;

  @override
  Widget build(BuildContext context) => RaisedButton(
    child: Text(character, style: TextStyle(fontSize: 30)),
    color: color,
    textColor: textcolor,
    onPressed: () => onPressed(character),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
  );
}
