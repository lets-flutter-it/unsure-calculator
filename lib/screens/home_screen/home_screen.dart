import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:unsurecalculator/blocs/calculator/calculator_bloc.dart';
import 'package:unsurecalculator/blocs/calculator/calculator_event.dart';
import 'package:unsurecalculator/model/user.dart';
import 'package:unsurecalculator/screens/home_screen/custom_drawer.dart';
import 'package:unsurecalculator/screens/home_screen/output_text.dart';

import 'keyboard_button.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen(this.user);

  final User user;
  final List<StaggeredTile> _staggeredTiles = <StaggeredTile>[
    const StaggeredTile.count(1, 0.7),
    const StaggeredTile.count(1, 0.7),
    const StaggeredTile.count(1, 0.7),
    const StaggeredTile.count(1, 0.7),
    ...List.generate(11, (index) => StaggeredTile.count(1, 1)),
    const StaggeredTile.count(1, 2),
    const StaggeredTile.count(2, 1),
    const StaggeredTile.count(1, 1),
  ];

  @override
  Widget build(BuildContext context) {
    final bloc = CalculatorBloc(this.user);
    return BlocProvider(
      create: (_) => bloc,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.orange),
        ),
        drawer: CustomDrawer(user),
        body: Padding(
          padding: const EdgeInsets.all(9),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              OutputText(),
              Spacer(flex: 3),
              SizedBox(
                height: MediaQuery.of(context).size.height * .67,
                width: double.infinity,
                child: StaggeredGridView.count(
                  primary: false,
                  crossAxisCount: 4,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 10,
                  children: [
                    ...['+', '-', '*', '/'].map(
                      (char) => KeyBoardButton(
                        char,
                        Colors.white,
                        Colors.orange,
                        onPressed: (char) => bloc.add(UpdateEquation(char)),
                      ),
                    ),
                    ...List.generate(
                      3,
                      (number) => KeyBoardButton(
                        '${number + 1}',
                        Colors.black,
                        Colors.white70,
                        onPressed: (char) => bloc.add(UpdateEquation(char)),
                      ),
                    ),
                    KeyBoardButton(
                      '~',
                      Colors.white,
                      Colors.orange,
                      onPressed: (char) => bloc.add(UpdateEquation(char)),
                    ),
                    ...List.generate(
                      3,
                      (number) => KeyBoardButton(
                        '${number + 4}',
                        Colors.black,
                        Colors.white70,
                        onPressed: (char) => bloc.add(UpdateEquation(char)),
                      ),
                    ),
                    KeyBoardButton(
                      'C',
                      Colors.white,
                      Colors.grey,
                      onPressed: (_) => bloc.add(ClearCalculator()),
                    ),
                    ...List.generate(
                      3,
                      (number) => KeyBoardButton(
                        '${number + 7}',
                        Colors.black,
                        Colors.white70,
                        onPressed: (char) => bloc.add(UpdateEquation(char)),
                      ),
                    ),
                    KeyBoardButton(
                      '=',
                      Colors.white,
                      Colors.grey[700],
                      onPressed: (_) => bloc.add(ShowResult()),
                    ),
                    ...['0', '.'].map(
                      (char) => KeyBoardButton(
                        char,
                        Colors.black,
                        Colors.white70,
                        onPressed: (char) => bloc.add(
                          UpdateEquation(char),
                        ),
                      ),
                    ),
                  ],
                  staggeredTiles: _staggeredTiles,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
