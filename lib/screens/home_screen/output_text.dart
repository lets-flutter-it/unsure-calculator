import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsurecalculator/blocs/calculator/calculator_bloc.dart';
import 'package:unsurecalculator/blocs/calculator/calculator_state.dart';

class OutputText extends StatelessWidget {
  const OutputText();

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CalculatorBloc, CalculatorState>(builder: (context, state) {
        return Column(
          children: [
            Text(
              state.equation,
              style: TextStyle(
                color: Colors.black38,
                fontSize: 25,
              ),
            ),
            SizedBox(height: 10),
            Text(
              state.result,
              style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        );
      });
}