import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_event.dart';
import 'package:unsurecalculator/model/user.dart';
import 'package:unsurecalculator/router/router.dart';
import 'package:unsurecalculator/screens/history.dart';
import 'package:unsurecalculator/widgets/button.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer(this.user);
  final User user;

  @override
  Widget build(BuildContext context) => Drawer(
        child: ListView(
          children: [
            DrawerHeader(child: Image.asset("assets/cal.png")),
            Row(
              children: [
              Icon(Icons.supervised_user_circle, color: Colors.orange, size: 30,),
              SizedBox(width: 22,),
               Text(user.name
                    .replaceAll("@unsurecalculator.com", "")
                    .replaceAll("_", " "),style: TextStyle(fontSize: 20)),
            ],),
            Divider(),
            Row(
              children: [
              Icon(Icons.history , color: Colors.orange),
               FlatButton(
                child: Text('History'),
                onPressed: () => Router.push(context,History(user.history))),
            ],),
            Divider(),
            Row(
              children: [
              Icon(Icons.exit_to_app , color: Colors.orange),
            FlatButton(
                child: Text('Log out'),
                onPressed: () => BlocProvider.of<AuthBloc>(context).add(Unauthenticate())),
            ],),
            Divider(),
          ],
        ),
      );
}
