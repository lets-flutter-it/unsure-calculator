import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_bloc.dart';
import 'package:unsurecalculator/blocs/login/logIn_bloc.dart';
import 'package:unsurecalculator/blocs/login/loginEvent.dart';
import 'package:unsurecalculator/blocs/login/loginState.dart';
import 'package:unsurecalculator/router/router.dart';
import 'package:unsurecalculator/util/snackbar.dart';
import 'package:unsurecalculator/widgets/button.dart';
import 'package:unsurecalculator/widgets/field.dart';
import 'register_screen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    final bloc = LoginBloc(BlocProvider.of<AuthBloc>(context));
  return BlocProvider(
  create: (_) => bloc,
  child: Scaffold(
  body: SingleChildScrollView(
  child: BlocConsumer<LoginBloc, LoginState>(
    listener: (BuildContext context, state) {
    if (state is LoginError)
    Snackbar.showError(context, state.errorData);
    },
    buildWhen: (previous, current) => current is! LoginError,
    listenWhen: (previous, current) => current is LoginError,
    builder: (BuildContext context, state) {
      return
       Column(
            children: [
              Image.asset(
                'assets/cal.png',
                height: 300,
              ),
              Field('Your user name...', Icons.account_box,bloc.userNameController),
              SizedBox(height: 15),
              Field('Your password...', Icons.lock,bloc.passwordController, obscure: true),
              SizedBox(height: 15),
              Button("Login", Colors.orange, Colors.white,onPressed:()=>bloc.add(LoginWithEmail())),
              Button("Register", Colors.white,Colors.grey,onPressed:()=>Router.push(context,RegisterScreen())),
            ],
          );}),
        ),
      ));}

}
