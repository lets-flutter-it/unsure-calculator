import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_bloc.dart';
import 'package:unsurecalculator/blocs/register/register_bloc.dart';
import 'package:unsurecalculator/blocs/register/register_event.dart';
import 'package:unsurecalculator/blocs/register/register_state.dart';
import 'package:unsurecalculator/router/router.dart';
import 'package:unsurecalculator/util/snackbar.dart';
import 'package:unsurecalculator/widgets/button.dart';
import 'package:unsurecalculator/widgets/field.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final registerBloc = RegisterBloc(BlocProvider.of<AuthBloc>(context));
    return BlocProvider(
        create: (_) => registerBloc,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            elevation: 0,
            iconTheme: IconThemeData(color: Colors.orange),
          ),
          body: SingleChildScrollView(
            child: BlocConsumer<RegisterBloc, RegisterState>(
              listener: (BuildContext context, state) {
                if (state is RegisterError)
                  Snackbar.showError(context, state.error);
              },
              buildWhen: (previous, current) => current is! RegisterError,
              listenWhen: (previous, current) => current is RegisterError,
              builder: (BuildContext context, state) {
                return Column(
                  children: [
                    Image.asset(
                      'assets/cal.png',
                      height: 250,
                    ),
                    SizedBox(height: 29),
                    Field('Your user name...', Icons.account_box,
                        registerBloc.userNameController),
                    SizedBox(height: 5),
                    Field('Your password...', Icons.lock,
                        registerBloc.passwordController,
                        obscure: true),
                    SizedBox(height: 30),
                    if (state is Loading)
                      CircularProgressIndicator()
                    else
                      Button("Register", Colors.white, Colors.grey,
                          onPressed: () => registerBloc.add(Register())),
                  ],
                );
              },
            ),
          ),
        ));
  }
}
