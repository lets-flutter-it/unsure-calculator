import 'package:flutter/material.dart';

abstract class Router {
  static void push(BuildContext context, Widget page) =>
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => page));
}

