import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_event.dart';
import 'package:unsurecalculator/blocs/login/loginEvent.dart';
import 'package:unsurecalculator/model/user.dart';
import 'package:unsurecalculator/util/validation.dart';

import 'loginState.dart';
import 'loginState.dart';
import 'loginState.dart';
import 'loginState.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'loginState.dart';
class LoginBloc extends Bloc<LoginEvent,LoginState>{
  final AuthBloc authBloc;

  LoginBloc(this.authBloc);
  @override
  // TODO: implement initialState
  LoginState get initialState => InitialState();
  final userNameController = TextEditingController(),
      passwordController = TextEditingController();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    String userName=userNameController.text+"@unsurecalculator.com",
    password=passwordController.text;

    if(event is LoginEvent){
      if(!Validator.validateFullName(userNameController.text)){

        userNameController.clear();
        yield LoginError('Invalid Email');
        yield InitialState();
        return;
      }else{
        userName=userName.replaceAll(" ", "_");
      }
      if (!Validator.validatePassword(password)) {
        passwordController.clear();
        yield LoginError('Invalid Password');
        yield InitialState();
        return;
      }

    }
    final error = LoginError('Invalid email or password...');
    yield Loding();
    try {
      final result = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: userName,
        password: password,
      );
      final uid = result.user.uid;
      final doc = await Firestore.instance.document('user/$uid').get();
      if (!doc.exists) {
        await FirebaseAuth.instance.signOut();
        yield error;
      }
      authBloc.add(Authenticate(User.fromJson( {...doc.data,"id":uid} )));
    } catch (e) {
      print(e);
      yield error;
    }
    yield InitialState();

  }
  @override
Future<Function> close(){
    super.close();
    userNameController?.dispose();
    passwordController?.dispose();
}
  }