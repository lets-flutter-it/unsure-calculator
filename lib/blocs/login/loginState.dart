abstract class LoginState{
  const LoginState();
}
class InitialState extends LoginState{

}
class Loding extends LoginState{

}
class LoginError extends LoginState{
  const LoginError( this.errorData );
  final String errorData;

}