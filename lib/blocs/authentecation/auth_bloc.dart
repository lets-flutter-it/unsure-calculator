import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:unsurecalculator/model/user.dart';

import 'auth_event.dart';
import 'auth_bloc_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  @override
  AuthState get initialState => LoadingApp();
  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    yield* event.when(
      startApp: () async* {
        final currentUser = await FirebaseAuth.instance.currentUser();
        print(currentUser?.email);

        if (currentUser == null) {
          yield Unauthenticated();
          return;
        }

        final uid = currentUser.uid;
        final doc = await Firestore.instance.document('user/$uid').get();
        if (!doc.exists) {
          await FirebaseAuth.instance.signOut();
          yield Unauthenticated();
          return;
        }
        final user = User.fromJson(doc.data);
        yield Authenticated(user);
      },
      authenticate: (user) async* {
        yield Authenticated(user);
      },
      unauthenticate: () async* {
        yield Unauthenticated();
        await FirebaseAuth.instance.signOut();
      },
    );
    
  }
}
