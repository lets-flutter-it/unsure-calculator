abstract class RegisterState{}
class InitialState extends RegisterState{}
class Loading extends RegisterState{}
class RegisterError extends RegisterState{
  final String error;
  RegisterError(this.error);
}
