import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_bloc.dart';
import 'package:unsurecalculator/blocs/authentecation/auth_event.dart';
import 'package:unsurecalculator/blocs/register/register_event.dart';
import 'package:unsurecalculator/blocs/register/register_state.dart';
import 'package:unsurecalculator/model/user.dart';
import 'package:unsurecalculator/util/validation.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthBloc authBloc;
  RegisterBloc(this.authBloc);
  final userNameController = TextEditingController(),
      passwordController = TextEditingController();

  @override
  RegisterState get initialState => InitialState();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    String username = userNameController.text, password = passwordController.text;

    if (event is Register) {
      
      if (!Validator.validateFullName(userNameController.text)) {
        userNameController.clear();
        yield RegisterError("Invalid Username");
        yield InitialState();
        return;
      }else{
        username=username.replaceAll(" ", "_")+"@unsurecalculator.com";
      }
      if (!Validator.validatePassword(password)) {
        passwordController.clear();
        yield RegisterError("Invalid password");
        yield InitialState();
        return;
      }
      try {
        yield Loading();
        final result = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
                email: username, password: password);
        final uid = result.user.uid;
        final user = User(name: username, id: uid);

        await Firestore.instance.document('user/$uid').setData(user.toJson());
        final doc = await Firestore.instance.document('user/$uid').get();
        authBloc.add(Authenticate(User.fromJson( {...doc.data,"id":uid} )));
      } catch (e) {
        yield RegisterError("Username Already In use");
        yield InitialState();
        return;
      }
    }
  }
}
