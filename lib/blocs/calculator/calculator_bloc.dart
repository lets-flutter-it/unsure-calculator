import 'package:bloc/bloc.dart';
import 'package:unsurecalculator/blocs/calculator/calculator_event.dart';
import 'package:unsurecalculator/blocs/calculator/calculator_state.dart';
import 'package:unsurecalculator/util/calculator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../model/user.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  final calculator = Calculator();
   CalculatorBloc(this.user);
  final User user;

  @override
  CalculatorState get initialState => CalculatorState();

  bool keepWriting = false;

  @override
  Stream<CalculatorState> mapEventToState(CalculatorEvent event) async* {
    if (event is UpdateEquation) {
      yield CalculatorState(
          equation: (keepWriting ? state.equation : '') + event.char);
      keepWriting = true;
    }
    if (event is ShowResult) {
      keepWriting = false;
      try {
        final result = calculator.execute(state.equation);
        yield CalculatorState(equation: state.equation, result: result);
        user.history.add(state.equation+"="+result);
        print(user.id);
        await Firestore.instance.collection('user')
            .document(user.id).updateData({"history":user.history});
      } catch (error) {
        print(error);
        yield CalculatorState(equation: state.equation, result: 'ERROR');
      }
    }

    if (event is ClearCalculator) yield CalculatorState();
  }
}
