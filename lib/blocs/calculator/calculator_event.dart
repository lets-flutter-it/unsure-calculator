abstract class CalculatorEvent {
  const CalculatorEvent();
}

class UpdateEquation extends CalculatorEvent {
  const UpdateEquation(this.char);
  final String char;
}

class ClearCalculator extends CalculatorEvent {}

class ShowResult extends CalculatorEvent {}
