class CalculatorState {
  const CalculatorState({this.equation = '', this.result = ''});
  final String equation, result;
}
